﻿using Humanizer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DnD.Data.Extensions
{
    public enum QueryOperators
    {
        Equal,
        NotEqual,
        StartsWith,
        EndsWith,
        Like,
        More,
        Less,
        MoreOrEqual,
        LessOrEqual,
    };

    public class QueryCondition
    {
        public string Property { get; set; }

        public QueryOperators Operator { get; set; }

        public object Value { get; set; }
    }

    public static class DbSetExtensions
    {
        public static IQueryable<TEntity> Query<TEntity>(this DbSet<TEntity> dbSet, params QueryCondition[] conditions)
            where TEntity : class
        {
            if (conditions == null || conditions.Length == 0)
                return dbSet;

            var wheres = new List<string>();
            var parameters = new List<object>();

            for (int i = 0; i < conditions.Length; i++)
            {
                var condition = conditions[i];
                wheres.Add(GetSqlConditionMapping(condition, i));
                parameters.Add(GetParameterMapping(condition));
            }

            var sql = $"SELECT * FROM {typeof(TEntity).Name.Pluralize()} WHERE {String.Join(" AND ", wheres)}";

            return dbSet.FromSqlRaw(sql, parameters.ToArray());
        }

        private static object GetParameterMapping(QueryCondition condition)
        {
            switch (condition.Operator)
            {
                case QueryOperators.Equal:
                case QueryOperators.NotEqual:
                case QueryOperators.More:
                case QueryOperators.Less:
                case QueryOperators.MoreOrEqual:
                case QueryOperators.LessOrEqual:
                    return condition.Value;
                case QueryOperators.StartsWith:
                    return $"{condition.Value}%";
                case QueryOperators.EndsWith:
                    return $"%{condition.Value}";
                case QueryOperators.Like:
                    return $"%{condition.Value}%";
                default:
                    return null;
            }
        }

        private static string GetSqlConditionMapping(QueryCondition condition, int index)
        {
            switch (condition.Operator)
            {
                case QueryOperators.Equal:
                    return condition.Property + " = {" + index + "}";
                case QueryOperators.NotEqual:
                    return condition.Property + " != {" + index + "}";
                case QueryOperators.StartsWith:
                case QueryOperators.EndsWith:
                case QueryOperators.Like:
                    return condition.Property + " LIKE {" + index + "}";
                case QueryOperators.More:
                    return condition.Property + " > {" + index + "}";
                case QueryOperators.Less:
                    return condition.Property + " < {" + index + "}";
                case QueryOperators.MoreOrEqual:
                    return condition.Property + " >= {" + index + "}";
                case QueryOperators.LessOrEqual:
                    return condition.Property + " <= {" + index + "}";
                default:
                    return null;
            }
        }
    }
}

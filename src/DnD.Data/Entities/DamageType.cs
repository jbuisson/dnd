using System;

namespace DnD.Data.Entities
{
    public enum DamageType
    {
        Bludgeoning,
        Piercing,
        Slashing,

        Acid,
        Cold,
        Fire,
        Force,
        Lightning,
        Necrotic,
        Poison,
        Psychic,
        Radiant,
        Thunder,
    }
}
using System;

namespace DnD.Data.Entities
{
    public enum Race
    {
        HillDwarf,
        MountainDwarf,
        HighElf,
        WoodElf,
        Drow,
        LightfootHalfling,
        StoutHalfling,
        Human,
        Dragonborn,
        ForestGnome,
        RockGnome,
        HalfElf,
        HalfOrc,
        Tielfing,
    }
}
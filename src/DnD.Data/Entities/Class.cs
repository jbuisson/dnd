using System;

namespace DnD.Data.Entities
{
    public enum Class
    {
        Barbarian,
        Paladin,
        Rogue,
        Ranger,
        Warlock,
        Wizard
    }
}
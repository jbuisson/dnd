﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DnD.Data.Entities
{
    public class Character
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Race Race { get; set; }

        public Class Class { get; set; }

        public int Experience { get; set; }

        public int HitPoints { get; set; }

        public int Strength { get; set; }

        public int Dexterity { get; set; }

        public int Constitution { get; set; }

        public int Intelligence { get; set; }

        public int Wisdom { get; set; }

        public int Charisma { get; set; }
    }
}

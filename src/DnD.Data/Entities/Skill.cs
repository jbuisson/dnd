using System;

namespace DnD.Data.Entities
{
    public enum Skill
    {
        Athletics,

        Acrobatics,
        SleightOfHand,
        Stealth,

        Arcana,
        History,
        Investigation,
        Nature,
        Religion,

        AnimalHandling,
        Insight,
        Medicine,
        Perception,
        Survival,

        Deception,
        Intimidation,
        Performance,
        Persuasion,
    }
}
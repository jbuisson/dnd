﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DnD.Data
{
    public class CharactersContextFactory : IDesignTimeDbContextFactory<CharactersContext>
    {
        private readonly IConfiguration m_configuration;

        public CharactersContextFactory()
        {
            m_configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false)
                .Build();
        }

        public CharactersContextFactory(IConfiguration configuration)
        {
            m_configuration = configuration;
        }

        public CharactersContext CreateDbContext(string[] args)
        {
            var password = m_configuration.GetValue<string>("ENTITIES_PASSWORD");
            var connectionString = m_configuration.GetConnectionString("Characters.Entities").Replace("$password", password);

            var builder = new DbContextOptionsBuilder<CharactersContext>()
                .UseMySql(connectionString, b => b.MigrationsAssembly(GetType().Assembly.FullName));

            var context = new CharactersContext(builder.Options);
            context.Database.EnsureCreated();
            return context;
        }
    }
}

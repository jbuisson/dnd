﻿using DnD.Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace DnD.Data
{
    public class CharactersContext : DbContext
    {
        public DbSet<Character> Characters { get; set; }

        public CharactersContext(DbContextOptions<CharactersContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Character>()
                .HasIndex(character => character.Name)
                .IsUnique();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DnD.Api.Models.Characters;
using DnD.Data.Entities;

namespace DnD.Api.Services
{
    public static class AbilityService
    {
        public static int GetCharacterModifier(CharacterModel character, Ability ability)
        {
            switch (ability)
            {
                case Ability.Strength:
                    return character.Strength.Modifier;
                case Ability.Dexterity:
                    return character.Dexterity.Modifier;
                case Ability.Constitution:
                    return character.Constitution.Modifier;
                case Ability.Intelligence:
                    return character.Intelligence.Modifier;
                case Ability.Wisdom:
                    return character.Wisdom.Modifier;
                case Ability.Charisma:
                    return character.Charisma.Modifier;
                default:
                    return 0;
            }
        }

        public static int GetRaceModifier(Race race, Ability ability)
        {
            switch (ability)
            {
                case Ability.Strength:
                    return GetRaceStrengthModifier(race);
                case Ability.Dexterity:
                    return GetRaceDexterityModifier(race);
                case Ability.Constitution:
                    return GetRaceConstitutionModifier(race);
                case Ability.Intelligence:
                    return GetRaceIntelligenceModifier(race);
                case Ability.Wisdom:
                    return GetRaceWisdomModifier(race);
                case Ability.Charisma:
                    return GetRaceCharismaModifier(race);
                default:
                    return 0;
            }
        }

        public static int GetRaceStrengthModifier(Race race)
        {
            switch (race)
            {
                case Race.Human:
                    return 1;
                case Race.MountainDwarf:
                case Race.HalfOrc:
                case Race.Dragonborn:
                    return 2;
                default:
                    return 0;
            }
        }

        public static int GetRaceDexterityModifier(Race race)
        {
            switch (race)
            {
                case Race.Human:
                case Race.ForestGnome:
                    return 1;
                case Race.HighElf:
                case Race.WoodElf:
                case Race.Drow:
                case Race.LightfootHalfling:
                case Race.StoutHalfling:
                    return 2;
                default:
                    return 0;
            }
        }

        public static int GetRaceConstitutionModifier(Race race)
        {
            switch (race)
            {
                case Race.Human:
                case Race.StoutHalfling:
                case Race.RockGnome:
                case Race.HalfOrc:
                    return 1;
                case Race.MountainDwarf:
                case Race.HillDwarf:
                    return 2;
                default:
                    return 0;
            }
        }

        public static int GetRaceIntelligenceModifier(Race race)
        {
            switch (race)
            {
                case Race.Human:
                case Race.Tielfing:
                case Race.HighElf:
                    return 1;
                case Race.RockGnome:
                case Race.ForestGnome:
                    return 2;
                default:
                    return 0;
            }
        }

        public static int GetRaceWisdomModifier(Race race)
        {
            switch (race)
            {
                case Race.Human:
                case Race.HillDwarf:
                case Race.WoodElf:
                    return 1;
                default:
                    return 0;
            }
        }

        public static int GetRaceCharismaModifier(Race race)
        {
            switch (race)
            {
                case Race.Human:
                case Race.Drow:
                case Race.Dragonborn:
                case Race.LightfootHalfling:
                    return 1;
                case Race.HalfElf:
                case Race.Tielfing:
                    return 2;
                default:
                    return 0;
            }
        }
    }
}

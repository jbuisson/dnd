﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DnD.Api.Models.Characters;
using DnD.Data.Entities;

namespace DnD.Api.Services
{
    public static class HitPointService
    {
        public static int GetClassBaseValue(Class @class)
        {
            switch (@class)
            {
                case Class.Barbarian:
                    return 12;
                case Class.Paladin:
                    return 10;
                case Class.Rogue:
                    return 8;
                case Class.Ranger:
                    return 10;
                case Class.Warlock:
                    return 8;
                case Class.Wizard:
                    return 6;
                default:
                    return 0;
            }
        }
    }
}

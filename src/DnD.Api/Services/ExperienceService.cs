﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DnD.Api.Models.Characters;
using DnD.Data.Entities;

namespace DnD.Api.Services
{
    public static class ExperienceService
    {
        public static int GetLevel(int experience)
        {
            if (experience < 300) return 1;
            if (experience < 900) return 2;
            if (experience < 2700) return 3;
            if (experience < 6500) return 4;
            if (experience < 14000) return 5;
            if (experience < 23000) return 6;
            if (experience < 34000) return 7;
            if (experience < 48000) return 8;
            if (experience < 64000) return 9;
            if (experience < 85000) return 10;
            if (experience < 100000) return 11;
            if (experience < 120000) return 12;
            if (experience < 140000) return 13;
            if (experience < 165000) return 14;
            if (experience < 195000) return 15;
            if (experience < 225000) return 16;
            if (experience < 265000) return 17;
            if (experience < 305000) return 18;
            if (experience < 355000) return 19;
            return 20;
        }

        public static int GetProficiency(int experience)
        {
            return (int)Math.Floor((7 + GetLevel(experience)) / 4.0);
        }
    }
}

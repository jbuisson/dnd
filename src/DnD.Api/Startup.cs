using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using DnD.Data;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using NSwag;

namespace DnD.Api
{
    public class Startup
    {
        public string ASPNETCORE_ENVIRONMENT { get; }
        public string PLATFORM_ENVIRONMENT { get; }

        private readonly IConfiguration m_configuration;

        public Startup(IWebHostEnvironment environment)
        {
            ASPNETCORE_ENVIRONMENT = System.Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            PLATFORM_ENVIRONMENT = System.Environment.GetEnvironmentVariable("PLATFORM_ENVIRONMENT");

            m_configuration = new ConfigurationBuilder()
                .SetBasePath(environment.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{ASPNETCORE_ENVIRONMENT?.ToLower()}.json", optional: true)
                .AddJsonFile($"appsettings.{ASPNETCORE_ENVIRONMENT?.ToLower()}-{PLATFORM_ENVIRONMENT?.ToLower()}.json", optional: true)
                .AddEnvironmentVariables()
                .Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddLogging();

            services
                .AddCors(options =>
                {
                    options.AddPolicy("Development", builder => builder
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowAnyOrigin());

                    options.AddPolicy("Production", builder => builder
                        .WithOrigins("dnd.jbuisson.com")
                        .WithMethods("GET", "POST", "DELETE", "OPTIONS")
                        .WithHeaders("Content-Type"));
                });

            services
                .AddControllers()
                .AddNewtonsoftJson(configure =>
                {
                    configure.SerializerSettings.Converters.Add(new StringEnumConverter());
                    configure.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });

            services
               .AddScoped(provider => new CharactersContextFactory(m_configuration).CreateDbContext(null));

            services.AddOpenApiDocument();
        }

        public void Configure(IApplicationBuilder applicationBuilder, IWebHostEnvironment webHostEnvironment)
        {
            if (webHostEnvironment.IsDevelopment())
            {
                applicationBuilder.UseDeveloperExceptionPage();
                applicationBuilder.UseCors("Development");
            }
            else
            {
                applicationBuilder.UseCors("Production");
            }

            applicationBuilder
                .UseOpenApi(settings =>
                {
                    settings.PostProcess = (document, request) =>
                    {
                        if (!webHostEnvironment.IsDevelopment())
                            document.Schemes = new[] { OpenApiSchema.Https };

                        document.Info.Title = "Dungeons & Dragons API";
                        document.Info.Description = "An API to play the world most famous role playin game.";
                        document.Info.Contact = new OpenApiContact
                        {
                            Name = "Jeremy Buisson @ Gitlab",
                            Url = "https://gitlab.com/jbuisson/dnd",
                            Email = String.Empty,
                        };
                    };
                })
                .UseSwaggerUi3(options =>
                {
                    options.Path = String.Empty;
                })
                .UseReDoc()
                .UseRouting()
                .UseEndpoints(endpoints => endpoints.MapControllers());
        }
    }
}

﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using DnD.Data.Entities;

namespace DnD.Api.Models.Characters
{
    public class CharacterModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Race Race { get; set; }

        public Class Class { get; set; }

        public AbilityModel Strength { get; set; }

        public AbilityModel Dexterity { get; set; }

        public AbilityModel Constitution { get; set; }

        public AbilityModel Intelligence { get; set; }

        public AbilityModel Wisdom { get; set; }

        public AbilityModel Charisma { get; set; }

        public int ArmorClass { get; set; }

        public int Experience { get; set; }

        public int Level { get; set; }

        public int Proficiency { get; set; }

        public int HitPoints { get; set; }
    }
}

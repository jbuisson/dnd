﻿using DnD.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DnD.Api.Models.Characters
{
    public class AbilityModel
    {
        public int Value { get; set; }

        public int Modifier { get; set; }
    }
}

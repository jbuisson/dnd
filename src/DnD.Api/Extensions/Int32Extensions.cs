﻿using DnD.Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DnD.Api.Extensions
{
    public static class Int32Extensions
    {
        public static int ToModifier(this int @this)
        {
            return (int)Math.Floor((@this - 10) / 2.0);
        }
    }
}

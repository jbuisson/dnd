﻿using DnD.Data;
using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace DnD.Api.ValidationAttributes.Characters
{
    public class NameValidationAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var context = validationContext.GetService<CharactersContext>();
            var character = context.Characters.FirstOrDefault(c => c.Name == (string)value);

            if (character == null)
                return null;

            return new ValidationResult($"Be more original, '{value}' is already taken.");
        }
    }
}

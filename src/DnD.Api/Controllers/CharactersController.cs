﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using DnD.Api.Actions.Characters;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using DnD.Data;
using DnD.Data.Entities;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DnD.Api.Actions;
using DnD.Api.Models.Characters;
using DnD.Api.Services;
using DnD.Api.Extensions;
using DnD.Core;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;
using DnD.Data.Extensions;

namespace DnD.Api.Controllers
{
    [ApiController]
    [Route("api/characters")]
    public class CharactersController : ControllerBase
    {
        private static readonly IDictionary<string, QueryOperators> OPERATORS = new Dictionary<string, QueryOperators>
        {
            { "=", QueryOperators.Equal },
            { "==", QueryOperators.Equal },
            { "!=", QueryOperators.NotEqual },
            { "=~", QueryOperators.StartsWith },
            { "~=", QueryOperators.EndsWith },
            { "~~", QueryOperators.Like },
            { ">", QueryOperators.More },
            { "<", QueryOperators.Less },
            { ">=", QueryOperators.MoreOrEqual},
            { "<=", QueryOperators.LessOrEqual },
        };

        private static readonly string QUERYSTRING_FILTER_PATTERN = $"^(?<property>[a-zA-Z0-9_]+)(?<operator>({String.Join("|", OPERATORS.Keys)}))(?<parameter>[a-zA-Z0-9 -]+)$";

        private static readonly IDictionary<string, Func<CharactersController, int, JObject, Task<IActionOutcome>>> Actions = new Dictionary<string, Func<CharactersController, int, JObject, Task<IActionOutcome>>>
        {
            { nameof(Attack).ToLower(), async (controller, id, action) => await controller.Attack(id) },
            { nameof(Hit).ToLower(), async (controller, id, action) => await controller.Hit(id, action.ToObject<HitAction>()) },
            { nameof(Save).ToLower(), async (controller, id, action) => await controller.Save(id, action.ToObject<SaveAction>()) },
        };

        private readonly CharactersContext _context;
        private readonly ILogger<CharactersController> _logger;

        public CharactersController(ILogger<CharactersController> logger, CharactersContext context)
        {
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        public async Task<IEnumerable<CharacterModel>> Get()
        {
            var conditions = ParseQueryString(Request.QueryString.ToString());

            return (await _context.Characters.Query(conditions).ToListAsync()).Select(ToModel);
        }

        [HttpGet("count")]
        public async Task<int> Count()
        {
            var conditions = ParseQueryString(Request.QueryString.ToString());

            return await _context.Characters.Query(conditions).CountAsync();
        }

        [HttpGet("{character:int}")]
        public async Task<CharacterModel> Get(int character)
        {
            return ToModel(await _context.Characters.FindAsync(character));
        }

        [HttpPost]
        public async Task<CharacterModel> Create(CreateAction action)
        {
            //FIXME: generate slug from name instead of using ID
            // https://stackoverflow.com/questions/2920744/url-slugify-algorithm-in-c

            var strengthRoll = Dice.D6.Rolls(4, AbilityService.GetRaceModifier(action.Race, Ability.Strength));
            var dexterityRoll = Dice.D6.Rolls(4, AbilityService.GetRaceModifier(action.Race, Ability.Dexterity));
            var constitutionRoll = Dice.D6.Rolls(4, AbilityService.GetRaceModifier(action.Race, Ability.Constitution));
            var intelligenceRoll = Dice.D6.Rolls(4, AbilityService.GetRaceModifier(action.Race, Ability.Intelligence));
            var wisdomRoll = Dice.D6.Rolls(4, AbilityService.GetRaceModifier(action.Race, Ability.Wisdom));
            var charismaRoll = Dice.D6.Rolls(4, AbilityService.GetRaceModifier(action.Race, Ability.Charisma));

            var character = new Character
            {
                Name = action.Name,
                Class = action.Class,
                Race = action.Race,
                Experience = 0,
                HitPoints = HitPointService.GetClassBaseValue(action.Class),
                Strength = strengthRoll.Best(3).Sum(),
                Dexterity = dexterityRoll.Best(3).Sum(),
                Constitution = constitutionRoll.Best(3).Sum(),
                Intelligence = intelligenceRoll.Best(3).Sum(),
                Wisdom = wisdomRoll.Best(3).Sum(),
                Charisma = charismaRoll.Best(3).Sum(),
            };

            character.HitPoints += character.Constitution.ToModifier();

            var entry = _context.Add(character);
            await _context.SaveChangesAsync();

            _logger.LogDebug($"Strength: {strengthRoll} = {character.Strength}");
            _logger.LogDebug($"Dexterity: {dexterityRoll} = {character.Dexterity}");
            _logger.LogDebug($"Constitution: {constitutionRoll} = {character.Constitution}");
            _logger.LogDebug($"Intelligence: {intelligenceRoll} = {character.Intelligence}");
            _logger.LogDebug($"Wisdom: {wisdomRoll} = {character.Wisdom}");
            _logger.LogDebug($"Charisma: {charismaRoll} = {character.Charisma}");

            return ToModel(entry.Entity);
        }

        [HttpPatch("{character:int}")]
        public async Task<CharacterModel> Update(int character, UpdateAction action)
        {
            var entity = await _context.Characters.FindAsync(character);

            entity.Name = action.Name;

            await _context.SaveChangesAsync();

            return ToModel(entity);
        }

        [HttpPost("{character:int}/attack")]
        public async Task<AttackOutcome> Attack(int character)
        {
            var model = await Get(character);
            var roll = Dice.D20.Roll(model.Proficiency + model.Strength.Modifier);

            return new AttackOutcome
            {
                Roll = roll.Value,
                IsFumble = roll.IsFumble,
                IsCritical = roll.IsCritical,
            };
        }

        [HttpPost("{character:int}/hit")]
        public async Task<HitOutcome> Hit(int character, HitAction action)
        {
            var entity = await Get(character);
            var damage = Dice.Parse(action.Roll).Sum();
            var save = action.Save != null ? await Save(character, action.Save) : null;

            if (save?.Success ?? false)
                damage /= 2;

            entity.HitPoints = Math.Max(0, entity.HitPoints - damage);
            await _context.SaveChangesAsync();

            return new HitOutcome
            {
                Save = save,
                Damage = damage,
                IsDead = entity.HitPoints == 0,
            };
        }

        [HttpPost("{character:int}/save")]
        public async Task<SaveOutcome> Save(int character, SaveAction action)
        {
            var roll = Dice.D20.Roll();
            var entity = await Get(character);
            var bonus = AbilityService.GetCharacterModifier(entity, action.Ability);

            return new SaveOutcome
            {
                IsFumble = roll.IsFumble,
                IsCritical = roll.IsCritical,
                Success = (roll + bonus) > action.Difficulty || roll.IsCritical,
            };
        }

        [HttpPost("{character:int}/hit/_batch")]
        public async Task<IEnumerable<HitOutcome>> HitBatch(int character, IEnumerable<HitAction> actions)
        {
            return await Task.WhenAll(actions.Select(action => Hit(character, action)));
        }

        [HttpPost("_batch/hit")]
        public async Task<IDictionary<int, HitOutcome>> Batch(Batch<HitAction> batch)
        {
            var outcome = new Dictionary<int, HitOutcome>();

            foreach (var character in batch.Characters)
                outcome.Add(character, await Hit(character, batch.Action));

            return outcome;
        }

        [HttpPost("{character:int}/_batch")]
        public async Task<IDictionary<string, object>> Batch(int character, Dictionary<string, JObject> batch)
        {
            var outcome = new Dictionary<string, object>();

            foreach (var action in batch)
                outcome.Add(action.Key, await Actions[action.Key](this, character, action.Value));

            return outcome;
        }

        [HttpPost("_batch")]
        public async Task<IDictionary<int, IDictionary<string, object>>> Batch(Dictionary<int, Dictionary<string, JObject>> batch)
        {
            var outcome = new Dictionary<int, IDictionary<string, object>>();

            foreach (var character in batch)
            {
                outcome.Add(character.Key, new Dictionary<string, object>());

                foreach (var action in character.Value)
                    outcome[character.Key].Add(action.Key, await Actions[action.Key](this, character.Key, action.Value));
            }

            return outcome;
        }

        private static QueryCondition[] ParseQueryString(string queryString)
        {
            if (String.IsNullOrEmpty(queryString))
                return Array.Empty<QueryCondition>();

            return HttpUtility.UrlDecode(queryString.Trim('?'))
                .Split('&')
                .Select(f => Regex.Match(f, QUERYSTRING_FILTER_PATTERN))
                .Where(m => m.Success)
                .Select(m => new QueryCondition
                {
                    Operator = OPERATORS[m.Groups["operator"].Value],
                    Property = m.Groups["property"].Value,
                    Value = m.Groups["parameter"].Value,
                })
                .ToArray();
        }

        private static CharacterModel ToModel(Character character)
        {
            return new CharacterModel
            {
                Id = character.Id,
                Name = character.Name,
                Class = character.Class,
                Race = character.Race,
                Experience = character.Experience,
                HitPoints = character.HitPoints,
                Level = ExperienceService.GetLevel(character.Experience),
                Proficiency = ExperienceService.GetProficiency(character.Experience),
                ArmorClass = 10 + character.Dexterity.ToModifier(),
                Strength = new AbilityModel { Value = character.Strength, Modifier = character.Strength.ToModifier() },
                Dexterity = new AbilityModel { Value = character.Dexterity, Modifier = character.Dexterity.ToModifier() },
                Constitution = new AbilityModel { Value = character.Constitution, Modifier = character.Constitution.ToModifier() },
                Intelligence = new AbilityModel { Value = character.Intelligence, Modifier = character.Intelligence.ToModifier() },
                Wisdom = new AbilityModel { Value = character.Wisdom, Modifier = character.Wisdom.ToModifier() },
                Charisma = new AbilityModel { Value = character.Charisma, Modifier = character.Charisma.ToModifier() },
            };
        }
    }
}

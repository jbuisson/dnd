using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace DnD.Api.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("[controller]")]
    [ApiController]
    public class SystemController : ControllerBase
    {
        [HttpGet("healthcheck")]
        public int HealthCheck()
        {
            return 1;
        }

        [HttpGet("version")]
        public string Version()
        {
            if (System.IO.File.Exists("version.txt"))
                return System.IO.File.ReadAllText("version.txt");

            return "No version found";
        }
    }
}
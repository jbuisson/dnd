using System;
using System.Collections.Generic;

namespace DnD.Api.Actions.Characters
{
    public class SearchOutcome<T>
    {
        public int Total { get; set; }

        public IEnumerable<string> Suggestions { get; set; }

        public IEnumerable<T> Results { get; set; }
    }
}

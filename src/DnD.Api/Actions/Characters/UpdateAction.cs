﻿using System;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json.Converters;
using DnD.Data.Entities;
using DnD.Api.ValidationAttributes.Characters;

namespace DnD.Api.Actions.Characters
{
    public class UpdateAction
    {
        [Required]
        [StringLength(50, MinimumLength = 2)]
        [RegularExpression("^[a-zA-z ]+$")]
        [NameValidation]
        public string Name { get; set; }
    }
}

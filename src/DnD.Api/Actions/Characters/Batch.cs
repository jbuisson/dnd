using System;
using System.Collections.Generic;

namespace DnD.Api.Actions.Characters
{
    public class Batch<T>
    {
        public IEnumerable<int> Characters { get; set; }

        public T Action { get; set; }
    }
}

using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using DnD.Data.Entities;
using Newtonsoft.Json.Converters;

namespace DnD.Api.Actions.Characters
{
    public class CheckAction
    {
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public Skill Skill { get; set; }

        [Required]
        [Range(0, 30)]
        public int Difficulty { get; set; }
    }

    public class CheckOutcome : IActionOutcome
    {
        public int Roll { get; set; }

        public bool IsCritical { get; set; }
        
        public bool IsFumble { get; set; }
    }
}

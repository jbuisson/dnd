using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using DnD.Api.Services;
using DnD.Core;
using DnD.Data.Entities;
using Newtonsoft.Json.Converters;

namespace DnD.Api.Actions.Characters
{
    public class HitAction
    {
        [Required]
        [JsonConverter(typeof(StringEnumConverter))]
        public DamageType Type { get; set; }

        [Required]
        [RegularExpression(Dice.ROLLS_EXPRESSION)]
        public string Roll { get; set; }

        public SaveAction Save { get; set; }
    }

    public class HitOutcome : IActionOutcome
    {
        public bool IsDead { get; set; }
        
        public int Damage { get; set; }

        public SaveOutcome Save { get; set; }
    }
}

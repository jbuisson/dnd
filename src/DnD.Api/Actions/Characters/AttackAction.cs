using System;
using System.Text.Json.Serialization;
using Newtonsoft.Json.Converters;

namespace DnD.Api.Actions.Characters
{
    public class AttackOutcome : IActionOutcome
    {
        public int Roll { get; set; }

        public bool IsCritical { get; set; }
        
        public bool IsFumble { get; set; }
    }
}

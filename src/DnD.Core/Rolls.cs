﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace DnD.Core
{
    public class Rolls : IEnumerable<Roll>
    {
        private readonly int _bonus = 0;
        private readonly List<Roll> _rolls = new List<Roll>();

        public Rolls(IEnumerable<Roll> rolls, int bonus = 0)
        {
            _bonus = bonus;
            _rolls.AddRange(rolls);
        }

        public IEnumerator<Roll> GetEnumerator()
        {
            return _rolls.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _rolls.GetEnumerator();
        }

        public int Sum()
        {
            return _rolls.Sum(roll => roll) + _bonus;
        }

        public Roll Best()
        {
            var best = _rolls.OrderByDescending(roll => roll).First();
            return new Roll(best.Value, best.Dice, _bonus);
        }

        public Rolls Best(int count)
        {
            return new Rolls(_rolls.OrderByDescending(roll => roll).Take(count), _bonus);
        }

        public Roll Worst()
        {
            var worst = _rolls.OrderBy(roll => roll).First();
            return new Roll(worst.Value, worst.Dice, _bonus);
        }

        public Rolls Worst(int count = 1)
        {
            return new Rolls(_rolls.OrderBy(roll => roll).Take(count), _bonus);
        }

        public override string ToString()
        {
            return $"{String.Join(";", _rolls)}+{_bonus}";
        }
    }
}

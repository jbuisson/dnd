﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace DnD.Core
{
    public class Dice
    {
        public const string ROLLS_EXPRESSION = @"^(?<count>[0-9]*)(D|d)(?<dice>[0-9]+)((?:(?<operator>[+\/*-])(?<bonus>[0-9]+)))?$";
        private static readonly Regex _regex = new Regex(ROLLS_EXPRESSION);

        public static Dice D100 => new Dice(100);
        public static Dice D20 => new Dice(20);
        public static Dice D12 => new Dice(12);
        public static Dice D10 => new Dice(10);
        public static Dice D8 => new Dice(8);
        public static Dice D6 => new Dice(6);
        public static Dice D4 => new Dice(4);

        private static readonly Random _random = new Random();

        private readonly int _value;

        public Dice(int value)
        {
            _value = value;
        }

        public Roll Roll(int bonus = 0)
        {
            return new Roll(_random.Next(1, _value + 1), _value, bonus);
        }

        public Rolls Rolls(int count, int bonus = 0)
        {
            return new Rolls(Enumerable.Range(1, count).Select(i => Roll()), bonus);
        }

        public Roll RollAdvantage()
        {
            return Rolls(2).Best();
        }

        public Roll RollDisadvantage()
        {
            return Rolls(2).Worst();
        }

        public static Rolls Parse(string expression)
        {
            var match = _regex.Match(expression);

            if (!match.Success)
                throw new InvalidOperationException($"The expression [{expression}] is not a valid dice roll.");

            var count = Int32.Parse(match.Groups["count"].Value);
            var dice = Int32.Parse(match.Groups["dice"].Value);
            var @operator = match.Groups["count"]?.Value;
            
            Int32.TryParse(match.Groups["bonus"]?.Value, out var bonus);

            if (@operator == "-")
                bonus *= -1;

            return new Dice(dice).Rolls(count, bonus);
        }
    }
}

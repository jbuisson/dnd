﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;

namespace DnD.Core
{
    public struct Roll : IComparable<int>, IEquatable<int>, IComparable<Roll>, IEquatable<Roll>
    {
        private readonly int _rolled;

        public int Value => _rolled + Bonus;
        public int Bonus { get; }
        public int Dice { get; }

        public Roll(int value, int dice, int bonus = 0)
        {
            _rolled = value;
            Dice = dice;
            Bonus = bonus;
        }

        public bool IsFumble => _rolled == 1;

        public bool IsCritical => _rolled == Dice;

        public int CompareTo(int other)
        {
            return Value.CompareTo(other);
        }

        public bool Equals(int other)
        {
            return Value.Equals(other);
        }

        public bool Equals(Roll other)
        {
            return Value.Equals(other);
        }

        public static implicit operator int(Roll roll) => roll.Value;

        public int ToInt32() => Value;

        public override bool Equals(object obj)
        {
            return Value.Equals(obj);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override string ToString()
        {
            return $"{_rolled}+{Bonus}";
        }

        public int CompareTo(Roll other)
        {
            return CompareTo((int)other);
        }

        public static bool operator ==(Roll left, Roll right)
        {
            return left.Value == right.Value;
        }

        public static bool operator !=(Roll left, Roll right)
        {
            return left.Value != right.Value;
        }

        public static bool operator <(Roll left, Roll right)
        {
            return left.Value < right.Value;
        }

        public static bool operator <=(Roll left, Roll right)
        {
            return left.Value <= right.Value;
        }

        public static bool operator >(Roll left, Roll right)
        {
            return left.Value > right.Value;
        }

        public static bool operator >=(Roll left, Roll right)
        {
            return left.Value >= right.Value;
        }
    }
}

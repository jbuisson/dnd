#!/bin/bash
statuscode=$(curl -s -o /dev/null -w "%{http_code}" http://localhost/system/healthcheck)
if [ $statuscode -ne "200" ]; then exit 1; fi

